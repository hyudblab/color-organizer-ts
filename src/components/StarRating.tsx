import React from 'react'
import Star from './Star'

interface StarRatingProps{
    starsSelected: number,
    totalStars?: number,
    onRate: any
}

const StarRating: React.FC<StarRatingProps> = ({starsSelected=0, totalStars=5, onRate}) =>
    <div className="star-rating">
        {[...Array(totalStars)].map((n, i) =>
            <Star key={i}
                  selected={i<starsSelected}
                  onClick={() => onRate(i+1)}/>
        )}
        <p>별점: {starsSelected} / {totalStars}</p>
    </div>

export default StarRating
