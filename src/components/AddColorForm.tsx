import React, { FormEvent } from 'react'
import '../stylesheets/AddColorForm.scss'

interface FormProps {
    onNewColor: (title: string, color: string) => void
}

const AddColorForm: React.FC<FormProps> = ({ onNewColor }) => {

    let _title: HTMLInputElement | null, _color: HTMLInputElement | null

    const submit = (e: FormEvent) => {
        e.preventDefault()
        if (!_title || !_color) {
            console.log("??")
        }
        else {
            onNewColor(_title.value, _color.value)
            _title.value = ''
            _color.value = '#000000'
            _title.focus()
        }
    }

    return (
        <form className="add-color" onSubmit={submit}>
            <input ref={input => _title = input}
                type="text"
                placeholder="색 이름..." required />
            <input ref={input => _color = input}
                type="color" required />
            <button>추가</button>
        </form>
    )

}

export default AddColorForm
