import React from 'react'
import StarRating from './StarRating'
import '../stylesheets/Color.scss'

interface ColorProps{
    title: string,
    color: string,
    rating: number,
    onRemove: any,
    onRate: any,
}

const Color: React.FC<ColorProps> = ({ title, color, rating=0, onRemove, onRate}) =>
    <section className="color">
        <h1>{title}</h1>
        <button onClick={onRemove}>X</button>
        <div className="color"
             style={{ backgroundColor: color }}>
        </div>
        <div>
            <StarRating starsSelected={rating} onRate={onRate}/>
        </div>
    </section>

export default Color
