import React from 'react'
import '../stylesheets/Star.scss'

interface StarProps {
    selected: boolean
    onClick: any
}

const Star: React.FC<StarProps> = ({
    selected = false,
    onClick
}) => {
    return (
        <div className={(selected) ? "star selected" : "star"}
            onClick={onClick}>
        </div>)
}

export default Star
