import React from 'react'
import Color from './Color'
import '../stylesheets/ColorList.scss'

interface ColorListProps{
    colors: any[],
    onRate: any,
    onRemove: any
}

const ColorList: React.FC<ColorListProps> = ({ colors=[], onRate, onRemove }) =>
    <div className="color-list">
        {(colors.length === 0) ?
            <p>색이 없습니다. (색을 추가해 주세요)</p> :
            colors.map(color =>
                <Color key={color.id}
                       {...color}
                       onRate={(rating: number) => onRate(color.id, rating)}
                       onRemove={() => onRemove(color.id)} />
            )
        }
    </div>

export default ColorList
