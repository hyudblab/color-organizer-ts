export interface ColorItem {
    title: string,
    color: string,
    rating?: number,    
}